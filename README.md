# PlaniRando

The goal is to develop a tool that helps to planning hikes.

The technical project manager is Ulysse Durand and the developers are Benjamin Duhamel, Vincent Gardies, Gwendal Le Guennec and Adrien Dubois.

It was about using the LidarHD data to obtain information about the terrain (10pts/square meter) so the user can query a hiking path from point A to point B that is the shortest with a reasonable slope. The A* algorithm was used with some optimisations not to download too much terrain data.

This is in the context of the Integrated Project course in ENS Lyon in 2024, it recieved a grade of 18/20.

The report is [here](readme/rapport.pdf)

The slides are [here](readme/slides.pdf)

# Objectif

Le but du projet est de développer un outil qui permet de planifier ses randonnées. Ce genre d’outil existe déjà mais l’atout de ce projet est qu’il prend en compte la topologie du terrain. A terme, cela pourrait donc permettre à des personnes à mobilité réduite de dessiner leurs propres itinéraires, qui  ́evitent les segments à forts dénivelés. Le projet peut  ́egalement être utile pour trouver des itinéraires en roller,  ́evitant les pentes dangereuses.
PlaniRando s’appuie sur le projet LiDAR. Nous exploitons ensuite ces données open-source afin de calculer le chemin entre un point A et un point B trouvant le meilleur compromis entre distance et dénivelé.

# Installation des dépendances

* Installer les librairies python

Si poetry n'est pas installé, le faire

```
pip install poetry
```

Puis installer les libairies python

```
poetry install
```

* Avoir installé [pdal >= 2.4.3](https://pdal.io)

Pour ce faire, si conda n'est pas installé
```
mkdir -p ~/ miniconda3

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh

bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3 

rm -rf ~/miniconda3/miniconda.sh

~/miniconda3/bin/conda init bash ~/miniconda3/bin/conda init zsh
```
Et enfin installer pdal avec conda

```
conda install -c conda -forge pdal
```


# Utilisation

```
./run.sh
```

Ou manuellement:

Lancer le serveur depuis le dossier `programme`

```
cd programme

python3 server.py
```

Puis avec le navigateur, se rendre sur `localhost:8500`

Dessus, en cliquant on peut se créer un chemin, et en ne selectionnant que des arêtes dans le chemin, on peut appeler `Calculate easy path`
En attendant un peu, le chemin va se changer en plus court chemin sans trop de dénivelé.

# Ressources

* [Rapport](rapport/rapport.pdf) (qui explique le fonctionnement du projet) 

* Page du [Projet LIDAR HD d'IGN](https://geoservices.ign.fr/lidarhd)

* Page des [MNT (Carte d'altitude) résolution 25m](http://files.opendatarchives.fr/professionnels.ign.fr/bdalti/)

## Sur l'algo A*

* https://youtu.be/A60q6dcoCjw?si=FGjufot5ZsI1aew4

* https://www.youtube.com/watch?v=GC-nBgi9r0U

* visualisation : https://clementmihailescu.github.io/Pathfinding-Visualizer/#

