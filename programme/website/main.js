var map = L.map('map').setView([45.27, 6.05], 16);

var iconrouge = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});
var iconbleue = new L.Icon({
  iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

class Point {
  constructor(lat,lng,toplist=[], size=1) {
    this.lat = lat;
    this.lng = lng;
    this.size = size; //size of the point (0 : don't show it)

    this.idx = 0; //index of the point if in a list of points
    this.toplist = toplist; //if the point is in a list of points
    this.element;

    this.depend = [];


    this.selected = false;

    if (size > 0) {
        this.marker = L.marker([this.lat,this.lng], {draggable:'true', icon:iconrouge}).addTo(map);
        this.marker.on('moveend', (e) => {
          let res = this.marker.getLatLng();
          this.lat = res.lat;
          this.lng = res.lng;
          for (let i=0;i<this.depend.length;i++){
            let p = this.depend[i];
            p.straight();
          }
          updatepoints();
        });
    }

    this.updatecolor();
  }

  updatecolor() {
    if (this.size > 0) {
      if (this.selected) {
        this.marker.setIcon(iconrouge);
      } else {
        this.marker.setIcon(iconbleue);
      }
    }
  }

  select() {
    this.selected = !(this.selected);
    if (this.selected) {
      selected.push(this);
    }
    else {
      selected = selected.filter((e) => {return e!=this});
    }
    this.updatecolor();
    updatepoints();
  }

  del() {
    points = points.filter((e)=>{return e!=this});
    selected = selected.filter((e)=>{return e!=this});
    if (this.depend.length == 1) {
      let p = this.depend[0];
      p.straight();
      p.points = p.points.filter((e)=>{return e!=this});

      let autrepoint = p.points[0];
      autrepoint.depend = autrepoint.depend.filter((e)=>{return e!=p});

      paths = paths.filter((e) => {return e!=p});
      map.removeLayer(p.marker);
    }
    else if (this.depend.length == 2) {
      let p1 = this.depend[0];
      let p2 = this.depend[1];
      p1.straight();
      p2.straight();
      p1.points[1] = p2.points[1];
      p1.points[1].depend[0] = p1;

      paths = paths.filter((e)=>{return e!=p2});
      map.removeLayer(p2.marker);
      p1.updatecolor();

    }
    map.removeLayer(this.marker);
    updatepoints();
  }

  delfs() {
    points = points.filter((e)=>{return e!=this});
    selected = selected.filter((e)=>{return e!=this});
    if (this.depend.length == 1) {
      let p = this.depend[0];
      p.straight();
      p.points = p.points.filter((e)=>{return e!=this});
      let autrepoint = p.points[0];
      autrepoint.depend = autrepoint.depend.filter((e)=>{return e!=p});
      paths = paths.filter((e) => {return e!=p});
      map.removeLayer(p.marker);
    }
    else if (this.depend.length == 2) {
      let p1 = this.depend[0];
      let p2 = this.depend[1];
      p1.points = p1.points.concat(p2.points);
      p1.points[p1.points.length-1].depend[0] = p1;

      p1.updatecolor();

      paths = paths.filter((e)=>{return e!=p2});
      map.removeLayer(p2.marker);
    }
    map.removeLayer(this.marker);
    updatepoints();
  }
}

class Path {
  constructor(points = [], toplist = []) {
    this.points = points;

    this.idx = 0; //index of the path if in a list of paths
    this.toplist = toplist; //if the path is in a list of paths
    this.element;

    this.selected = false;

    this.updatecolor();
  }

  updatecolor() {
    let traductedpoints = this.points.map(
      (p,i) => {p.inx = i;return [p.lat,p.lng]}
    );
    if (this.selected) {
      if (this.marker) {
        map.removeLayer(this.marker);
      }
      this.marker = L.polyline(traductedpoints, {color:'red'}).addTo(map);
    }
    else {
      if (this.marker){
        map.removeLayer(this.marker);
      }
      this.marker = L.polyline(traductedpoints, {color:'blue'}).addTo(map);
    }
  }


  select() {
    this.selected = !(this.selected);
    if (this.selected) {
      selected.push(this);
    }
    else {
      selected = selected.filter((e) => {return e!=this});
    }
    this.updatecolor();
    updatepoints();
  }

  straight() {
    this.points = [this.points[0], this.points[this.points.length - 1]];
    this.updatecolor();
  }
}

const form = document.getElementById("form");

const divpoints = document.getElementById("points");

const addpoints = document.getElementById("addpoints");

const plist = document.getElementById("plist");

const allpoints = document.getElementById("allpoints");

const allpaths = document.getElementById("allpaths");

document.getElementById("unselectall").addEventListener("click",()=>{selected = []; updateselect();})

document.getElementById("selectallpoints").addEventListener("click",()=>{selected = points; updateselect();})

document.getElementById("selectalllines").addEventListener("click",()=>{selected = paths; updateselect();})

document.getElementById("apodeletesl").addEventListener("click",()=>{
  for (let i=0;i<selected.length;i++) {
    selected[i].del();
  }
});

document.getElementById("apodeletefs").addEventListener("click", ()=>{
  for (let i=0;i<selected.length;i++) {
    selected[i].delfs();
  }
});

document.getElementById("apastr").addEventListener("click", ()=>{
  for (let i=0;i<selected.length;i++) {
    selected[i].straight();
  }
});

async function getRequest(url = "", data = {}) {
  let vraiurl = url+"?";
  for (let key in data) {
    vraiurl = vraiurl + key +"="+ data[key]+"&";
  }
  console.log(vraiurl);

  const xhr = new XMLHttpRequest();
  xhr.open("POST", url);
  xhr.setRequestHeader("Content-Type","application/json; charset=UTF-8")
  xhr.onload = (() => {
    if (xhr.readyState == 4 && xhr.status == 200) {
      console.log(xhr.response);
    } else {
      console.log(`Error: ${xhr.status}`);
    }
  });
  xhr.send(JSON.stringify(data));
}

document.getElementById("apacalculate").addEventListener("click", () => {

  if (canweinterface() != 2) {
    console.error("Please select only lines to calculate a path.");
    return;
  }

  for (var i = 0; i < selected.length; i++) {
    const line = selected[i];
    const point1 = {
        lat: line.points[0].lat,
        lng: line.points[0].lng
    };
    const point2 = {
        lat: line.points[1].lat,
        lng: line.points[1].lng
    };
  
    // Send coordinates to server
    fetch('/calculatePath', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({point1, point2})
    })
    .then(response => response.json())
    .then(data => {
        // Handle response from server
        // Display calculated path on map
        let newpoints = [];
        for (let j=0;j<data.length;j++) {
          let lepoint = data[j];
          let lenouveaupoint = new Point(lepoint.lat,lepoint.lng,[] ,0)
          newpoints.push(lenouveaupoint);
          points.push(lenouveaupoint);
        }

	console.log(line.points);
        console.log("Received path from server:", data);
        line.points = newpoints;
        line.updatecolor();
        console.log(line.points);
    })
    .catch(error => {
        console.error('Error:', error);
    });
  }
});


function wetheraddpoints() {
  let res = addpoints.checked;
  return res;
}

function updateselect() {
  for (let i=0;i<paths.length;i++) {let e=paths[i]; e.selected = selected.includes(e); e.updatecolor();}
  for (let i=0;i<points.length;i++) {let e=points[i];e.selected = selected.includes(e); e.updatecolor();}
  updatepoints();
}

function canweinterface() {
  let res = 0;
  if (selected.every( (e) => e instanceof Path)) {res = 2;}
  if (selected.every( (e) => e instanceof Point)){res = 1;}
  if (selected.length == 0) { res = 0 ;}
  return res;
  //2 pour si c'est que des lignes, 1 si c'est pour que des points, 0 sinon
}

var points = [];
var paths = [];
var selected = [];

L.tileLayer('https://tile.opentopomap.org/{z}/{x}/{y}.png', {
}).addTo(map);


//L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
//}).addTo(map);


function displayPath(pathData) {
  // Create an array to store LatLng objects
  const latLngArray = [];

  // Loop through the coordinates and create LatLng objects
  pathData.forEach(coord => {
    const latLng = L.latLng(coord.lat, coord.lng);
    latLngArray.push(latLng);
  });

  // Create a polyline using the LatLng array
  const polyline = L.polyline(latLngArray, { color: 'green' }).addTo(map);

  // Optionally, fit the map bounds to the polyline
  map.fitBounds(polyline.getBounds());
}

function updatepoints() {
  plist.innerHTML = "";
  for (let i=0;i<points.length;i++) {
    let laradio = document.createElement("button");
    let textt = document.createTextNode(i);
    laradio.appendChild(textt);
    points[i].element = laradio;
    laradio.objet = points[i];
    laradio.setAttribute("onclick","this.objet.select();");
    if (points[i].selected) {laradio.style.backgroundColor ="red";}
    plist.appendChild(laradio);

    if (i < points.length-1) {
      let lechemin = document.createElement("button");
      let textt = document.createTextNode("->");
      lechemin.appendChild(textt);
      paths[i].element = lechemin;
      lechemin.objet = paths[i];
      lechemin.setAttribute("onclick","this.objet.select();");
      if (paths[i].selected) {lechemin.style.backgroundColor="red";}

      plist.appendChild(lechemin);
      plist.appendChild(document.createElement("br"));
    }
  }

  let wtd = canweinterface();
  if (wtd==0) {
    allpoints.hidden = true;
    allpaths.hidden = true;
  }
  else if (wtd == 1) {
    allpoints.hidden = false;
    allpaths.hidden = true;
  }
  else if (wtd == 2) {
    allpoints.hidden = true;
    allpaths.hidden = false;
  }
}

map.on('click', function(e){

  var coord = e.latlng;
  var lat = coord.lat;
  var lng = coord.lng;

  if (wetheraddpoints()) {
    let newpoint = new Point(lat,lng);
    points.push(newpoint);

    if (points.length > 1) {
      let pointA = points[points.length - 2];
      let pointB = newpoint;
      let lepath = new Path([pointA, pointB]);
      pointA.depend.push(lepath);
      pointB.depend.push(lepath);
      paths.push(lepath);
    }
  }
  updatepoints();
});
