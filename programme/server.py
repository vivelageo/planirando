import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import os

from modules.main import compute_path


class WebRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/":
            # Serve index.html
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            with open("website/index.html", "rb") as file:
                self.wfile.write(file.read())
        else:
            # Serve other files (e.g., CSS, JS)
            try:
                with open(os.getcwd()+ self.path, "rb") as file:
                    self.send_response(200)
                    if self.path.endswith(".css"):
                        self.send_header("Content-type", "text/css")
                    elif self.path.endswith(".js"):
                        self.send_header("Content-type", "application/javascript")
                    self.end_headers()
                    self.wfile.write(file.read())
            except FileNotFoundError:
                self.send_error(404, "File Not Found: %s" % self.path)

    def do_POST(self):
        if self.path == "/calculatePath":
            content_length = int(self.headers["Content-Length"])
            post_data = self.rfile.read(content_length)
            data = json.loads(post_data.decode("utf-8"))
            p1 = data["point1"]
            p2 = data["point2"]
            calculated_path = compute_path(p1,p2)

            # Return calculated path to client
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(json.dumps(calculated_path).encode("utf-8"))
        else:
            self.send_error(404, "Not Found")


if __name__ == "__main__":
    port = 8500
    server = HTTPServer(("0.0.0.0", port), WebRequestHandler)
    print(f"Server Launched on port {port}")
    server.serve_forever()
