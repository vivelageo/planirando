from pyproj import Transformer
import requests


# Convert lon/lat to lambert93 and lambert93 to lon/lat
def lon_lat_to_lambert93(lon, lat):
    # Convert lon/lat to Lambert 93
    transformer = Transformer.from_crs("EPSG:4326", "EPSG:2154")
    x,y = transformer.transform(lon, lat)
    return (x, y)


def lambert93_to_lon_lat(x, y):
    # Convert Lambert 93 to lon/lat
    transformer = Transformer.from_crs("EPSG:2154","EPSG:4326")
    lat, lon = transformer.transform(x,y)
    return {"lng": lon, "lat": lat}

def lambert93_to_lon_lat_list(points):
    return [lambert93_to_lon_lat(*point) for point in points]


def index_to_lambert93_list(nw, points, resolution):
    # given the first North-West points of a BDALTI tile, return the corresponding lambert93 of all given indexes
    xnw, ynw = nw
    points93 = [(xnw + x * resolution, ynw - y * resolution) for (x, y) in points]
    return points93


def lambert93_to_index_list(nw, points93, resolution):
    # given the first North-West points of a BDALTI tile, return the corresponding indexes of all given lambert93
    xnw, ynw = nw
    points = [(int((x - xnw) // resolution), int((ynw - y) // resolution)) for (x, y) in points93]
    return points


# Determine which tile coordinates are on
def lambert93_to_small_tile_LHD(x, y):
    # Obtain the 4 first digits of the lambert93 coordinates
    xs, ys = str(int(x / 1000)), str(int(y / 1000))
    # name format
    if len(xs) < 4:
        xs = "0" + xs
    square = "LHD_FXX_" + xs + "_" + ys + "_PTS_O_LAMB93_IGN69.copc.laz"
    return square


def lambert93_to_tile_BDALTI(x, y):
    # Obtain the 4 first digits of the lambert93 coordinates
    xs, ys = int((x + 13.5) / 1000), int((y - 12.5) / 1000 + 25)
    xs, ys = str(xs - xs % 25), str(ys - ys % 25)
    # name format
    # xllcorner, yllcorner = south-west
    # name of file : north-west
    if len(xs) < 4:
        xs = "0" + xs
    square = "BDALTIV2_25M_FXX_" + xs + "_" + ys + "_MNT_LAMB93_IGN69.asc"
    return square


def lambert93_to_big_tile(x, y):
    # AA->ZZ with 50x50km tiles
    xt, yt = x / 10000 - 10, y / 10000 - 600
    xT, yT = chr(ord("A") + xt / 5), chr(ord("A") + yt / 5)
    return xT + yT


# Request CURL to obtain the tile from LiDAR server


def download_file(url, destination):
    response = requests.get(url, stream=True)
    with open(destination, "wb") as file:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                file.write(chunk)


"""
# Example usage
url = "https://example.com/path/to/your/file.copc.laz"
destination_path = "local/path/to/save/file.copc.laz"
download_file(url, destination_path)
"""


def obtain_tile(x, y):
    url = "http://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/"

    download_file()

def translate_coord_raster(p1, resolution, polygon):
    # given a polygone and a resolution, return the indexes of the point of coordinates p1 in Lambert93 in the polygone
    min_x = min(polygon, key=lambda x: x[0])[0]
    max_y = max(polygon, key=lambda x: x[1])[1]
    return (min_x, max_y), int((p1[0]-min_x)/resolution), int((max_y-p1[1])/resolution)
