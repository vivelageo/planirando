from modules.coordinates_treatement import lon_lat_to_lambert93, translate_coord_raster, index_to_lambert93_list, lambert93_to_lon_lat_list, lambert93_to_index_list
from modules.wide_scale import large_rect
from modules.copc_request import request_COPC
from modules.recup_data.recup_link_list import get_dalle_from_polygon
from modules.tampon import tampon, convex_poly
from modules.astarrando import astarfinal, get_path

import random



def compute_path(p1, p2):
    """
    Compute the path between the points in arguments

    Args:
        p1 float*float Tuple: Coordinates of the starting point (in lag/long)
        p2 float*float Tuple: Coordinates of the arrival point (in lag/long)
    """
    
    p1_lamb = lon_lat_to_lambert93(p1["lat"], p1["lng"])
    p2_lamb = lon_lat_to_lambert93(p2["lat"], p2["lng"])
    
    print("Finding low precision raster...")
    low_precision_tile, nw_repere = large_rect([p1_lamb, p2_lamb])
    p1_index, p2_index = lambert93_to_index_list(nw_repere, [p1_lamb, p2_lamb], 25)
    print("Computing a path with low precision...")
    pts = get_path(p1_index, p2_index, low_precision_tile)
    
    print("Computing polygon...")
    polygon = tampon(pts, 25)
    polygon_convex = convex_poly(polygon)
    polygon_convex_lamb = index_to_lambert93_list(nw_repere, polygon_convex, 25)
    
    # À finir 
    # links_dalle = get_dalle_from_polygon(polygon_convex)
    
    links_dalle = ["https://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/QM/LHD_FXX_0939_6469_PTS_O_LAMB93_IGN69.copc.laz",
      "https://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/QM/LHD_FXX_0938_6469_PTS_O_LAMB93_IGN69.copc.laz",
      "https://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/QM/LHD_FXX_0938_6468_PTS_O_LAMB93_IGN69.copc.laz",
      "https://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/QM/LHD_FXX_0939_6468_PTS_O_LAMB93_IGN69.copc.laz"]

    print("Requesting COPC...")
    raster = request_COPC(polygon_convex_lamb, links_dalle)
    nw_repere2 = min(polygon_convex_lamb, key=lambda x:x[0])[0], max(polygon_convex_lamb, key=lambda x:x[1])[1]
    p1_img, p2_img = lambert93_to_index_list(nw_repere2, [p1_lamb, p2_lamb], 0.5)

    print("Computing final path...")
    final_path = astarfinal(raster, p1_img, p2_img)
    
    final_path_lambert = index_to_lambert93_list(nw_repere2, final_path, 0.5)
    final_path_lon_lag = lambert93_to_lon_lat_list(final_path_lambert)
    
    print("Optimal path computed.")
    return final_path_lon_lag
