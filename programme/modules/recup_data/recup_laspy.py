from recup_func import *

with laspy.open("data/dalle.laz") as fh:
    las = fh.read()
    N = len(las.points)
    n = 500  # taille de l'image n*n
    carte_alt = mat_mean_alt(n, las, speed=10)
    save_carte_csv(carte_alt)
    carte_alt = convert_carte_01(carte_alt, las, n)
    save_carte_elev(carte_alt, color_alt)
