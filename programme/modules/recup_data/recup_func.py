import laspy
import numpy as np
import sys
from PIL import Image
import csv

color_alt = [
    (255, 255, 204),
    (255, 237, 160),
    (254, 217, 118),
    (254, 178, 76),
    (253, 141, 60),
    (252, 78, 42),
    (227, 26, 28),
    (189, 0, 38),
    (128, 0, 38),
]


def scale_las(las_file):
    las_file.x = las_file.x * las_file.header.scales[0] + las_file.header.offsets[0]
    las_file.y = las_file.y * las_file.header.scales[1] + las_file.header.offsets[1]
    las_file.z = las_file.z * las_file.header.scales[2] + las_file.header.offsets[2]


def mat_mean_alt(n, las, speed=10):
    """
    Input : int n, carte las
    Output : matrice de taille n*n dont les coordonnées sont les moyennes des altitudes
    """
    x_min, y_min, z_min = tuple(las.header.mins.astype(np.int32))
    x_max, y_max, z_max = tuple(las.header.maxs.astype(np.int32))
    carte_alt = np.zeros((n, n), dtype=int)
    nbr_points_par_pixel = np.zeros((n, n), dtype=int)
    N = len(las)
    for i in range(0, N, speed):
        if i % (N // 100) == 0:
            sys.stdout.write("\r")
            sys.stdout.write(
                "[%-20s] %d%%" % ("#" * ((20 * i) // (N - 1)), (100 * i) // (N - 1))
            )
            sys.stdout.flush()
        x = int(n * (las["X"][i] // 100 - x_min) / (x_max - x_min + 1))
        y = int(n * (las["Y"][i] // 100 - y_min) / (y_max - y_min + 1))
        # carte_alt[x, y] += las['Z'][i]//100
        # nbr_points_par_pixel[x, y] += 1
        if carte_alt[x, y] == 0:
            carte_alt[x, y] = las["Z"][i]
        else:
            carte_alt[x, y] = min(carte_alt[x, y], las["Z"][i])
    for x in range(n):
        for y in range(n):
            if carte_alt[x, y] == 0:
                carte_alt[x, y] = z_min * 100
    sys.stdout.write("\r")
    sys.stdout.write("[%-20s] %d%%" % ("#" * 20, 100))
    sys.stdout.flush()
    sys.stdout.write("\n")
    return carte_alt


def convert_carte_01(carte_alt, las, n):
    carte_alt = carte_alt.astype("float64")
    z_min = las.header.mins.astype(np.int32)[2]
    z_max = las.header.maxs.astype(np.int32)[2]
    for i in range(n):
        for j in range(n):
            carte_alt[i, j] = max((carte_alt[i, j] // 100 - z_min) / (z_max - z_min), 0)
    return carte_alt


def color_from_color_alt(p, color_alt):
    m = len(color_alt)
    ind_c = min(m - 2, int((m - 1) * p))
    t = (m - 1) * p - ind_c
    return tuple(
        [
            int(t * (b - a) + a)
            for (a, b) in [
                (color_alt[ind_c][i], color_alt[ind_c + 1][i]) for i in [0, 1, 2]
            ]
        ]
    )


def save_carte_elev(
    carte_alt, color_alt, nom="data/pictures/carte_alt", normalize=False, n=500
):
    if normalize:
        carte_alt = (1 / max(carte_alt.ravel())) * carte_alt
    carte_alt_inv = np.ones((len(carte_alt), len(carte_alt))) - carte_alt
    elevation_matrix = (carte_alt_inv * 255).astype(np.uint8)
    # Noir & Blanc
    elevation_image_NB = Image.new("L", (n, n))
    elevation_image_NB.putdata(elevation_matrix.ravel())
    elevation_image_NB.save(nom + "_NB.png")
    # Couleurs
    elevation_image_C = Image.new("RGB", (n, n))
    for i in range(n):
        for j in range(n):
            elevation_image_C.putpixel(
                (i, j), color_from_color_alt(carte_alt[j, i], color_alt)
            )
    elevation_image_C.save(nom + "_color.png")


def save_carte_csv(carte_alt, file="data/cartes/carte_alt"):
    with open(file + ".csv", mode="w", newline="") as f:
        w = csv.writer(f)
        for ligne in carte_alt:
            w.writerow(ligne)


def infos(las):
    N = len(las.points)
    print("=== INFOS ===")
    print(
        "* Points from data:",
        N,
        "(environ {}x{})".format(int(np.sqrt(N)), int(np.sqrt(N))),
    )
    print("* Scales : ", las.points.scales, las.header.scales[0])
    print("* Offsets : ", las.points.offsets, las.header.offsets[0])
    print("* Format : ", list(las.point_format.dimension_names))
    print("=============")
