import matplotlib.pyplot as plt
import requests
from bs4 import BeautifulSoup
import pickle
from pathlib import Path


def create_links_list():
    url = "https://storage.sbg.cloud.ovh.net/v1/AUTH_63234f509d6048bca3c9fd7928720ca1/ppk-lidar/"
    try:
        page = requests.get(url)
    except:
        print("Connection failed: No internet connection ?")
        return None
    soup = BeautifulSoup(page.text, "html.parser")
    
    a_tags = soup.find_all('a')

    # Extract text from each <a> tag
    links_text = [a.text for a in a_tags]

    # Remove last element 
    links_text.pop() 
    
    links = []
    
    
    example_last = "LAMB93_IGN69.copc.laz"
    
    for sub_dalle in links_text:
        try:
            page = requests.get(url + sub_dalle)
        except:
            print("Connection failed: No internet connection ?")
            return None
        soup = BeautifulSoup(page.text, "html.parser")
        a_tags = soup.find_all('a')
        links_text = [(url + sub_dalle + a.text) for a in a_tags if len(a.text) > len(example_last) and a.text[-len(example_last):] == example_last]
        links += links_text[2:-3]
        
        print(f"Subpage processed {sub_dalle}")

    test_last = "LAMB93_IGN69.copc.laz"
    error = False
    
    for dalle in links:
        if url != dalle[:len(url)] or test_last != dalle[-len(test_last):]:
            print(f"Error : {dalle}")
            error = True
            
    if not error:
        file = Path(__file__).resolve().parent.parent.parent / "data/list_dalles_links"
        if not file.parent.exists():
            file.parent.mkdir(parents=True)
        file.touch(exist_ok=True)
        with open(file, "wb") as f:
            pickle.dump(links, f, pickle.HIGHEST_PROTOCOL)
        
        print("File saved !")
        print(f"Number of url : {len(links)}")
        
    

# Already done ! (long: 293574 urls)
# create_links_list()


def recup_links_list():
    file = Path(__file__).resolve().parent.parent.parent / "data/list_dalles_links"
    if not file.parent.exists():
        file.parent.mkdir(parents=True)
    file.touch(exist_ok=True)
    with open(file, "rb") as f:
        try:
            links = pickle.load(f)
        except EOFError:
            links = None
    return links   

def get_dalle_from_polygon(polygon):
    return None
