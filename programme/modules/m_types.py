from typing import List, Tuple

ElevationMatrix = List[List]

LambPoint = Tuple[float, float]
IndexPoint = Tuple[int, int]
