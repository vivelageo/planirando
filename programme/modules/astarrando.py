import astar
import cv2
from math import sqrt


def get_path(s1, s2, M, option: int = 0):
    """runs astar on the map"""
    # s1 is the start point
    # s2 is the destination
    # M is the elevation matrix
    # option is an optional parameter to precise the difficulties of the hiking

    n, m = M.shape

    def distance(p1, p2):
        """computes the distance between two points"""
        return sqrt(4 * (p2[1] - p1[1]) ** 2 + 4 * (p2[0] - p1[0]) ** 2)

    def denivele(p1, p2, option):
        # Compute the absolute value of the denivele between two points
        # Option enables us to add weight on that value
        match option:
            case 0:
                weight = 20000
            case 1:
                weight = 10
            case 2:
                weight = 1
        try:
            weight * abs(M[p1[0], p1[1]] - M[p2[0], p2[1]]) // 10
        except:
            print(f"p1: {p1},p2: {p2}, M:  {M}")
        return weight * abs(M[p1[0], p1[1]] - M[p2[0], p2[1]]) // 10

    def neighbors(p):
        # input : a point (x,y)
        # outputs : its neighbors in the grid
        x, y = p
        return [
            (x + dx, y + dy)
            for dx in [-1, 0, 1]
            for dy in [-1, 0, 1]
            if 0 <= x + dx <= n - 1 and 0 <= y + dy <= m - 1 and not dx == dy == 0
        ]

    return list(astar.find_path(
        s1,
        s2,
        neighbors_fnct=neighbors,
        heuristic_cost_estimate_fnct=lambda x, y: distance(x, y),
        distance_between_fnct=lambda x, y: distance(x, y) + denivele(x, y, option),
    ))

def astarfinal(imgname, p1, p2, opt=0):
    mat = cv2.imread(imgname, cv2.IMREAD_ANYDEPTH).T
    return get_path(p1, p2, mat, option=opt)
