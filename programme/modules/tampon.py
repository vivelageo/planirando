import numpy as np
from math import sqrt
from scipy.spatial import ConvexHull

def in_carre(point, carre):
    # input :
    #  point : (int,int)
    #  carre : list of 4 int
    # output : bool, does point belong to carre ?
    (x, y) = point
    return (
        x <= carre[0][0] and y <= carre[0][1] and x >= carre[2][0] and y >= carre[2][1]
    )


def classe(x, y, xt, yt, xp, yp):
    # Return 0 if (xp, yp) is on the right of the path and 1 else
    if (xt - x) * (xp - x) + (yt - y) * (yp - y) >= 0:
        return 0
    else:
        return 1


def tampon(path, epaisseur):
    # Input :
    #  path : chemin ou liste de points
    #  épaisseur : épaisseur du tampon carré : multiple de 25 car en mètre

    # Output :
    #  polygone : liste de points qui entoure le chemin

    liste_point = []
    liste_carre = []
    dico_classe = {}
    e = epaisseur // 25
    l = len(path)

    # premier point
    (x, y) = path[0]
    # On tanponne avec un carré (ce carré possède des points au milieu des arêtes pour avoir un beau polygone)
    carre = [
        (x, y + e),
        (x + e, y + e),
        (x + e, y),
        (x + e, y - e),
        (x, y - e),
        (x - e, y - e),
        (x - e, y),
        (x - e, y + e),
    ]
    # On calcule les coordonnée du vecteur tangent (xt, yt)
    (xs, ys) = path[e]
    (xt, yt) = (x + int((-ys + y) * 10), y + int((xs - x) * 10))
    # On classifie les points du carré :
    for xp, yp in carre:
        dico_classe[(xp, yp)] = classe(x, y, -xt, -yt, xp, yp)
    liste_point.append(carre)
    carre = [(x + e, y + e), (x + e, y - e), (x - e, y - e), (x - e, y + e)]
    liste_carre.append(np.copy(carre))

    # autres points:
    for i in range(e, l - 1, e):
        point = path[i]
        (x, y) = point
        # On tanponne avec un carré
        carre = [(x + e, y + e), (x + e, y - e), (x - e, y - e), (x - e, y + e)]
        carre_copy = np.copy(carre)
        liste_carre.append(carre_copy)
        # On supprime les points d'anciens carrés qui tombent dans le nouveau
        # On supprime les points du nouveau qui tombent dans d'anciens carrés
        for j in range(max(0, len(liste_carre) - 4), len(liste_carre) - 1):
            for angle in carre:
                if in_carre(angle, liste_carre[j]):
                    carre.remove(angle)
            for angle in liste_point[j]:
                if in_carre(angle, carre_copy):
                    liste_point[j].remove(angle)
        liste_point.append(carre)
        (xs, ys) = path[i - e]
        # On calcule les coordonnée du vecteur tangent (xt, yt)
        if i + e < l:
            (xo, yo) = path[i + e]
            (xt, yt) = (x + int((-ys + yo) * 10), y + int((xs - xo) * 10))
        else:
            (xt, yt) = (x + int((-ys + y) * 10), y + int((xs - x) * 10))
        (x, y) = point
        # On classifie les points restants du carré
        for xp, yp in carre:
            dico_classe[(xp, yp)] = classe(x, y, xt, yt, xp, yp)

    # dernier point (même principe que pour le premier)
    (x, y) = path[-1]
    carre = [
        (x, y + e),
        (x + e, y + e),
        (x + e, y),
        (x + e, y - e),
        (x, y - e),
        (x - e, y - e),
        (x - e, y),
        (x - e, y + e),
    ]
    carre_copy = [(x + e, y + e), (x + e, y - e), (x - e, y - e), (x - e, y + e)]
    liste_carre.append(carre_copy)
    for j in range(max(0, len(liste_carre) - 4), len(liste_carre) - 1):
        for angle in carre:
            if in_carre(angle, liste_carre[j]):
                carre.remove(angle)
        for angle in liste_point[j]:
            if in_carre(angle, carre_copy):
                liste_point[j].remove(angle)
    liste_point.append(carre)
    (xs, ys) = path[i - e]
    (xt, yt) = (x + int((-ys + y) * 10), y + int((xs - x) * 10))
    for xp, yp in carre:
        dico_classe[(xp, yp)] = classe(x, y, xt, yt, xp, yp)

    # Rassemble tous les points
    l = []
    for sous_liste in liste_point:
        l += sous_liste
        
    return l


def convex_poly(polygon):
    # Calculer l'enveloppe convexe
    hull = ConvexHull(polygon)

    indices = hull.vertices

    # Récupérer les points de l'enveloppe convexe
    enveloppe_convexe = [polygon[i] for i in indices] + [polygon[indices[0]]]
    return enveloppe_convexe
