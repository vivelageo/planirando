import json
import modules.recup_data.recup_link_list as recup_list
import subprocess

output_filename = "tmp/outputfile.tif"
json_file = "tmp/pipeline.json"


def formatpolygon(p):
    res = "POLYGON (("
    tojoin = []
    for point in p:
        tojoin.append("{a:.3f} {b:.3f}".format(a=point[0],b=point[1]))
    res += ",".join(tojoin)
    res += "))"
    return res


def request_COPC(polygon_shape, links=None):
    
    links = links if links is not None else recup_list.recup_links_list()
    
    polyform = formatpolygon(polygon_shape)
    lejson = []
    
    for link in links:
        lejson.append({
    	"type" : "readers.copc",
    	"polygon" : polyform,
    	"filename" : link
        })
    
    lejson.append({
    	"type" : "writers.gdal",
    	"filename" : output_filename,
    	"output_type" : "min",
    	"resolution" : "0.5"
    })
    
    with open(json_file, 'w') as f:
        json.dump(lejson, f, indent=4)

    # Execute the PDAL pipeline using the pdal pipeline command in the terminal
    print("Executing Pdal...")
    subprocess.run(['pdal', 'pipeline', json_file])
    return output_filename
