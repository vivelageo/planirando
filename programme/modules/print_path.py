from astarrando import get_path
from PIL import Image
import matplotlib.pyplot as plt
import csv
import numpy as np
from tampon import tampon

"""
Ce programme affiche et sauvegarde le chemin en 3D, en 2D et le profil de la rando
On a aussi une superposition de chemins possibles en 2D.
"""

data = "data/cartes/carte_alt.csv"

# Plot params
color_map = "YlOrBr"  # Gradient, choix sur https://matplotlib.org/stable/users/explain/colors/colormaps.html
_3D_fig = "3Dpath.png"  # Name of the file
_profil_fig = "profil.png"
contour_bool = False  # Projection of contours on the ground
projection_path = False  # Projection du chemin au sol
show3D = True  # Matplotlib popup
precision = 400  # Quality of the images

## Calculs

# Chargement des données
with open(data, mode="r", newline="") as f:
    r = csv.reader(f)
    carte_elev = np.array([[int(alt) for alt in line] for line in r])

# Coordonnées en pixel des points de départ et d'arrivée (origine en haut à gauche)
A, B = (10, 10), (460, 460)  # (0,len(mat_elev[0])-1), (len(mat_elev)-1,0)


# Calcul du chemin
path0 = get_path(A, B, carte_elev, 0)
path_copy0 = [(b, a) for (a, b) in path0]
"""
path1 = get_path(A, B, carte_elev,1)
path_copy1 = [(b,a) for (a,b) in path1]

path2 = get_path(A, B, carte_elev,2)
path_copy2 = [(b,a) for (a,b) in path2]
"""
path_copy = path_copy0
path = path0
## Saves & Plot
"""
# 2D:
with Image.open('data/pictures/carte_alt_color.png') as im:
    for (x, y) in path_copy0:
        im.putpixel((x, y), (0, 0, 0))
    for (x, y) in path_copy1:
        im.putpixel((x, y), (100, 0, 0))
    for (x, y) in path_copy2:
        im.putpixel((x, y), (0, 100, 0))
    im.save("data/pictures/2Dpath_superposed.png")
"""

with Image.open("data/pictures/carte_alt_color.png") as im:
    for x, y in path_copy:
        im.putpixel((x, y), (0, 0, 0))
    im.save("data/pictures/2Dpath.png")

polygone = tampon(path_copy, 225)

with Image.open("data/pictures/carte_alt_color.png") as im:
    for x, y in path_copy:
        im.putpixel((x, y), (0, 0, 0))
    for x, y in polygone:
        if x > 0 and x < 500 and y > 0 and y < 500:
            im.putpixel((x, y), (0, 0, 100))
    im.save("data/pictures/2Dpath_lombrifie.png")

"""
# Part to show the path and the polygon
xl = [point[0] for point in polygone]
yl = [point[1] for point in polygone]
xl2=[point[0] for point in path_copy0]
yl2=[point[1] for point in path_copy0]
# Plot the polygon
plt.plot(xl, yl,label='polygon') 
plt.plot(xl2,yl2,label='path')
plt.title('Polygon from Points')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend()
plt.grid(True)
plt.show()
"""

"""
# 3D:
#Attention ! Affichage qui inverse selon les x. Pas de conservation nord-sud
def draw_3Dpath(ax, path):
    x_path = [point[0] for point in path]
    y_path = [point[1] for point in path]
    z_path = [carte_elev[b, a]/100 for (a, b) in path]
    ax.scatter(x_path, y_path, z_path, c='black', s=1, alpha=1, depthshade=False)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x = np.arange(carte_elev.shape[1])
y = np.arange(carte_elev.shape[0])
X, Y = np.meshgrid(x, y)
Z = (1/100) * carte_elev 
min_alt = min([Z[a, b] for a in x for b in y])

ax.plot_surface(X, Y, Z, cmap=color_map)
if contour_bool:
    ax.contour(X, Y, Z, zdir='z', offset=min_alt-50, cmap='coolwarm')
if projection_path:
    ax.scatter([x for (x,_) in path_copy], [y for (_,y) in path_copy], zs=min_alt, zdir='z', color='black', s=1)

draw_3Dpath(ax, path_copy)
ax.view_init(elev=45, azim=70)
plt.savefig('data/pictures/'+_3D_fig, format='png', dpi=precision)

if show3D:
    plt.show()
plt.close()
"""

"""
#Calcul et tracé du profil de la rando
def draw_profil(path_copy):
    def distance(p1, p2):
        #computes the distance between two points
        return sqrt(4*(p2[1]-p1[1])**2+4*(p2[0]-p1[0])**2)
     
        
    x=[0]
    z=[carte_elev[y,x]//100 for (x,y) in path_copy]
    m=min(z)
    z=[e-m for e in z]
    dp=0
    dn=0
    list_color=[]
    diff=-1
    ndiff=-1
    for i in range(len(path_copy)-1):
        x.append(x[i]+distance(path_copy[i],path_copy[i+1]))
        d=z[i+1]-z[i]
        if (d >0):
            dp+=d
        else:
            dn-=d
    for i in range(len(path_copy)-5):
        d=z[i+5]-z[i]
        pourc=100*abs(d/distance(path_copy[i],path_copy[i+5]))
        
        if (pourc<8):
            ndiff='green'
        elif (pourc<15):
            ndiff='blue'
        elif (pourc<25):
            ndiff='yellow'
        elif (pourc<35):
            ndiff='red'
        else:
            ndiff='black'
        if (ndiff!=diff):
            list_color.append((i,ndiff))
            diff=ndiff
        if i==len(path_copy)-5:
            list_color.append((len(path_copy)-1),'white')
    ind=0
    sup=0
    list_color_copy=list_color.copy()
    for j in range(1,len(list_color_copy)-1) :
        i=list_color_copy[j][0]
        if (i-ind)<=8:
            del list_color[j-sup-1]
            sup+=1
        ind=i
    plt.figure()
    ind=0
    colo=list_color[0][1]
    for (i,diff) in list_color[1::] :
        plt.bar(x[ind:i],z[ind:i],width=3,color=colo)
        ind=i
        colo=diff
    plt.title("Profil de la randonnée :\nDénivelé positif : "+str(dp)+"m\nDénivelé négatif : "+str(dn)+"m")

    print("Dénivelé positif : "+str(dp)+"m")
    print("Dénivelé négatif : "+str(dn)+"m")
    plt.show()
    plt.savefig('data/pictures/'+_profil_fig, format='png')
draw_profil(path_copy)
"""
