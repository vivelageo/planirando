import numpy as np


def lambert93rounded_to_tile_BDALTI(x, y):  # return the file given rounded coordinates
    xs, ys = str(x), str(y)
    if len(xs) < 4:
        xs = "0" + xs
    square = "BDALTIV2_25M_FXX_" + xs + "_" + ys + "_MNT_LAMB93_IGN69.asc"
    return square


def asc_to_matrix(filename):  # Create a matrix from .asc file in data/cartes directory
    matrix = []
    with open("data/cartes/"+filename, "r") as file:
        for _ in range(6):  # Skip the first 6 lines
            next(file)
        for line in file:
            row = list(
                map(float, line.strip().split())
            )  # Split by space and convert to float
            matrix.append(row)
    return matrix


def concatenate_matrix_of_matrices(
    matrix_of_matrices,
):  # concatenate a matrix of matrix into a single matrix
    concatenated_matrix = []
    for submatrix_row in matrix_of_matrices:
        # Assuming all submatrices have the same number of rows
        submatrix_rows = len(submatrix_row[0])
        for i in range(submatrix_rows):
            concatenated_row = []
            for submatrix in submatrix_row:
                concatenated_row.extend(submatrix[i])
            concatenated_matrix.append(concatenated_row)
    return concatenated_matrix


def large_rect(pts):
    # Take a list of points in Lambert93 and return the matrix which contains all points for low accuracy A*

    # Select the extrema of the list : max_x = (x maximum, y), max_y = (x, maximum y)
    max_x, min_x, max_y, min_y = pts[0][0], pts[0][0], pts[0][1], pts[0][1]
    for pt in pts:
        max_x = max(max_x, pt[0])
        min_x = min(min_x, pt[0])
        max_y = max(max_y, pt[1])
        min_y = min(min_y, pt[1])

    # Extrema tiles (xllcorner, yllcorner = south-west)
    # +13.5 and +25 offset due to the difference between the name of file and the origine of the map in the file
    # 0.3 factor applied to the scale of a tile (%25) 
    # to have a rectangle slightly larger than the chosen points
    # and we add max(825), etc... to ensure the rectangle stays within the departement
    nw = min_x, max_y
    xsw = max(825, int((min_x - (min_x%25)*0.3 + 13.5) / 1000))
    ysw = max(6475, int((min_y - (min_y%25)*0.3 - 12.5) / 1000 + 25))
    xsw, ysw = xsw - xsw % 25, ysw - ysw % 25

    xne = min(950, int((max_x + (max_x%25)*0.3 + 13.5) / 1000))
    yne = max(6500, int((max_y + (max_y%25)*0.3 - 12.5) / 1000 + 25))
    xne, yne = xne - xne % 25, yne - yne % 25

    # matrix of all coordinates needed for the big rectangle tile
    all_tiles_coord = [[(xi, yne)] for xi in range(xsw, xne + 25, 25)]
    for i in range(len(all_tiles_coord)):
        for yi in range(ysw, yne + 25, 25):
            all_tiles_coord[i].append((all_tiles_coord[i][0][0], yi))

    # matrix of all tiles needed
    all_tiles = [
        [asc_to_matrix(lambert93rounded_to_tile_BDALTI(pt[0], pt[1])) for pt in all_tiles_coord[i]]
        for i in range(len(all_tiles_coord))
    ]

    # concatenated tile
    massive_tile = concatenate_matrix_of_matrices(all_tiles)

    # return the concatenated rectangle of the wide tile, and the coordinates of the north-west point of the rectangle
    return np.array(massive_tile), nw
